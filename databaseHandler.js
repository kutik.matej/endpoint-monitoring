const config = require('./mySQL.config'),
    mysql = require('mysql'),
    async = require('async');
    
module.exports = {insert, getBy, remove, update, getOne, getAll, getByLimit}

function connect(callback) {
    var con = mysql.createConnection(config);
    
    con.connect(err => {
        if (err)  {
            return create(callback);
        }
        callback(null, con)
    });
}

function create(callback) {
    let {host, user, password, database} = config;
    var con = mysql.createConnection({host, user, password});

    async.waterfall([
        step => con.connect(err => step(err)),
        step => con.query(`CREATE DATABASE ${database}`, err => step(err)),
        connect,
        (newCon, step) =>
            async.parallel([
                next => createMonitoredEndpoint(newCon, next),
                next => createMonitoringResult(newCon, next),
                next => createUsers(newCon, next)
            ], err => step(err, newCon))
            
    ], callback);
}

function createMonitoringResult(con, callback) {
    createTable(
        con,
        "MonitoringResult",
        "id INT AUTO_INCREMENT PRIMARY KEY, createdAt DATETIME, httpStatus INT, payload VARCHAR(255), monitoredEndpointId INT",
        callback)
}

function createMonitoredEndpoint(con, callback) {
    createTable(
        con,
        'MonitoredEndpoint',
        "id INT AUTO_INCREMENT PRIMARY KEY, name VARCHAR(255), url VARCHAR(255), monitoredInterval INT, createdAt DATETIME, lastCheck DATETIME, owner INT",
        callback)
}

function createUsers(con, callback) {

    async.waterfall([
        step => createTable(con, "User", "id INT AUTO_INCREMENT PRIMARY KEY, name VARCHAR(255), email VARCHAR(255), accessToken VARCHAR(255)", err => step(err)),
        step => insert("User", {name: 'Jablotron', email: 'info@jablotron.cz', accessToken: "93f39e2f-80de-4033-99ee-249d92736a25"}, err => step(err)),
        step => insert("User", {name: 'Batman', email: 'batman@example.com', accessToken: "dcb20f8a-5657-4f1b-9f7f-ce65739b359"}, err => step(err))
    ], callback)
}

function createTable(con, name, structure, callback) {
    con.query(`CREATE TABLE ${name} (${structure})`, err => callback(err));
}

function insert(tableName, data, callback) {
    var keys = values = '(';

    var objectKeys = Object.keys(data)
    for(var i = 0; i < objectKeys.length; i++) {
        keys = `${keys} ${objectKeys[i]}${i === (objectKeys.length - 1) ? ")" : ", "}`;
        values = `${values} '${data[objectKeys[i]].toString()}'${i === (objectKeys.length - 1) ? ")" : ", "}`;
    }
    
    connect((err, con) => {
        if (err) return callback(err);

        var sql = `INSERT INTO ${tableName} ${keys} VALUES ${values}`;
        
        con.query(sql, function (err, result) {
          if (err) return callback(err);
        
          con.end();
          callback(null, result.insertId)
        });
    })
}

function getAll(tableName, callback) {
    connect((err, con) => {
        if (err) return callback(err);

        con.query(`SELECT * FROM ${tableName}`, (...args) => {
            callback(...args)
            con.end()
        });
    })
}

function getBy(tableName, data, callback) {
    connect((err, con) => {
        if (err) return callback(err);

        var key = Object.keys(data)[0];
        if (!key || !data[key])
            return callback(true)
        
        con.query(`SELECT * FROM ${tableName} WHERE ${key} = '${data[key]}'`, (...args) => {
            callback(...args)
            con.end()
        });
    })
}

function getOne(tableName, data, callback) {
    getBy(tableName, data, (err, doc) => callback(err, doc ? doc[0] : doc));
}

function remove(tableName, data, callback) {
    connect((err, con) => {
        if (err) return callback(err);

        var syntax = getSyntax(data, " AND")
        
        if (!syntax)
            return callback(true)

        con.query(`DELETE FROM ${tableName} WHERE ${syntax}`, (...args) => {
            callback(...args)
            con.end()
        });
    })
}

function update(tableName, dataWhere, newData, callback) {
    connect((err, con) => {
        if (err) return callback(err);

        let set = getSyntax(newData, ",");
        let where = getSyntax(dataWhere, " AND");
        if (!where || !set)
            return callback(true)

        con.query(`UPDATE ${tableName} SET ${set} WHERE ${where}`, (...args) => {
            callback(...args)
          con.end();
        });
    })
}

function getSyntax(data, separator) {
    var keys = Object.keys(data);
    
    if (!keys[0] || !data[keys[0]])
        return;

    var syntax = `${keys[0]} = '${data[keys[0]]}'`

    for (let i = 1; i < keys.length; i++) {
        if (keys[i] && data[keys[i]])
            syntax = `${syntax}${separator} ${keys[i]} = '${data[keys[i]]}'`
    }
    
        return syntax;
}

function getByLimit(tableName, data, limit, sortBy, callback) {
    connect((err, con) => {
        if (err) return callback(err);

        var key = Object.keys(data)[0];
        
        con.query(`SELECT * FROM ${tableName} WHERE ${key} = '${data[key]}' ORDER BY ${sortBy} LIMIT ${limit}`, (...args) => {
            callback(...args)
            con.end()
        });
    })
}