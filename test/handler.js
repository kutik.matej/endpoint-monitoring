const handler = require('../handler');

describe("create", () => {
    it("Should return id(int)", done => {
        var data = {name: "Url test", url: "https://www.jablotron.com/cz/", monitoredInterval: 5000}
        handler.create(1, data, done)
    })

    it("Should return return an error 403", done => {
        var data = {name: "Url test", url: "https://www.jablotron.com/cz/"}
        handler.create(1, data, (err, res) => {
            if (err)
                return done();
            done(true);
        })
    })

    it("Should return return an error 403", done => {
        var data = {name: "Url test", url: "https://www.jablotron.com/cz/", monitoredInterval: 0}
        handler.create(1, data, (err, res) => {
            if (err)
                return done();
            done(true);
        })
    })

    it("Should return return an error 403", done => {
        var data = {name: "Url test", monitoredInterval: 0}
        handler.create(1, data, (err, res) => {
            if (err)
                return done();
            done(true);
        })
    })

    it("Should return return an error 403", done => {
        var data = {url: "https://www.jablotron.com/cz/", monitoredInterval: 0}
        handler.create(1, data, (err, res) => {
            if (err)
                return done();
            done(true);
        })
    })
})

describe("getAll", () => {
    it("Should return Aray", done => {
        handler.getAll(1, done)
    })

    it("Should return return an empty array", done => {
        var data = {name: "Url test", url: "https://www.jablotron.com/cz/"}
        handler.getAll(5, (err, res) => {
            if (!res[0])
                return done();
            done(err || res);
        })
    })
})

describe("getAll", () => {
    it("Should return User model", done => {
        handler.getUser("93f39e2f-80de-4033-99ee-249d92736a25", done)
    })

    it("Should return User model", done => {
        handler.getUser("dcb20f8a-5657-4f1b-9f7f-ce65739b359", done)
    })

    it("Should return return undefined", done => {
        var data = {name: "Url test", url: "https://www.jablotron.com/cz/"}
        handler.getUser("486ea668--das669s", (err, res) => {
            console.log(err || res);
            
            done(err || res);
        })
    })
})

describe("remove", () => {
    it("Should return {success: true}", done => {
        handler.remove(1, 1, done)
    })

    it("Should return {success: true}", done => {
        handler.remove(2, undefined, done)
    })
})

describe('update', () => {
    it('Should return {success: true}', done => {
        var data = {name: "Url test", url: "https://www.jablotron.com/cz/", monitoredInterval: 5000}
        handler.update(1, 20, data, done)
    })

    it('Should return {success: true}', done => {
        var data = {name: "Url test"}
        handler.update(1, 35, data, done)
    })

    it('Should return error', done => {
        var data = {}
        handler.update(1, 0, data, err => {
            done(err ? undefined : true);
        })
    })

    it('Should return error', done => {
        var data = {}
        handler.update(1, 20, data, err => done(err ? null : true))
    })
})

describe('getResults', () => {
    it('Should return Array', done => {
        handler.getResults(1, 20, done)
    })

    it('Should return {success: true}', done => {
        var data = {name: "Url test"}
        handler.getResults(1, undefined, err=> done(err ? null : true))
    })
})