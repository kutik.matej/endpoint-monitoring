const db = require('./databaseHandler'),
    moment = require('moment'),
    AskerHandler = require("./AskerHandler")();

module.exports = { create, getAll, getUser, remove, update, getResults }

function create(userId, {name, url, monitoredInterval}, callback) {
    if (!monitoredInterval || monitoredInterval < 1 || !name || !url)
        return callback({code: 403})
    db.insert('MonitoredEndpoint', {
        name,
        url,
        monitoredInterval,
        createdAt: moment().format("YYYY-MM-DD HH:mm:ss"),
        owner: userId
    }, (err, id) => {
        callback(err, id);
        if (err) return;
        
        AskerHandler.addById(id);
    })
}

function getAll(userId, callback) {
        db.getBy('MonitoredEndpoint', {owner: userId}, callback)
}

function getUser(accessToken, callback) {
    db.getOne('User', {accessToken}, callback)
}

function remove(userId, endPointId, callback) {
    if (!endPointId)
        return callback();

    db.remove('MonitoredEndpoint', {owner: userId, id: endPointId}, err => {
        callback(err);
        if (!err) 
            AskerHandler.remove(endPointId);
    })
}

function update(userId, endPointId, {name, url, monitoredInterval}, callback) {
    if (!endPointId)
        return callback({code: 403})
    var data = {}
    if (name) data.name = name;
    if (url) data.url = url;
    if (monitoredInterval) data.monitoredInterval = monitoredInterval;

    db.update("MonitoredEndpoint", {owner: userId, id: endPointId}, data, err => {
        callback(err);

        if (!err)
            AskerHandler.update(endPointId);
    })
}

function getResults(UserId, urlId, callback) {
    db.getOne("MonitoredEndpoint", {id: urlId}, (err, urlData) => {
        if (err) return callback(err);
        if (!urlData) return callback({code: 403});
        if (urlData.owner !== UserId) return callback({code: 403});

        db.getByLimit('MonitoringResult', {monitoredEndpointId: urlId}, 10, "createdAt DESC", callback)
    })
}