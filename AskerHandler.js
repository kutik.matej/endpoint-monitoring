const db = require('./databaseHandler'),
    Asker = require('./asker');

class AskerHandler {
    constructor() {
        this.askers = {};
    }
    // get all url and start send requests
    start() {
        db.getAll('MonitoredEndpoint', (err, data) => {
            if (err) throw err;
            for(var item of data)
                this.add(item);
        });
    }
    // Get url by id and start send requests
    addById(id) {
        db.getOne('MonitoredEndpoint', {id}, (err, data) => {
            if (!err && data)
                this.add(data);
        })
    }
    // Find Asker by id, stop it ask and remove from object collection
    remove(id) {
        if (this.askers[id]) {
            var A = this.askers[id];

            if (A.stop)
                A.stop();
            delete this.askers[id];
        }
    }
    //  Create Asker, make it ask a add it to object collection
    add(data) {
        if (!this.askers[data.id] && data.id) {
            var A = new Asker(this.onError, data.id, data.monitoredInterval, data.url);
            A.go();
            this.askers[data.id] = A;
        }
    }
    // Remove Asker and create new one
    update(id) {
        this.remove(id);
        this.addById(id);
    }
    // Error handler for Asker
    onError(A, err) {
        console.error(`${A.id} => ${err}`);
        A.stop();
        A.go();
    }
}

var Handler = new AskerHandler()

module.exports = () => Handler;