const handler = require('./handler');

module.exports = server => {
      server.use((req, res, next) => {
        // handle error response
        req.errorHandler = (err, statusCode = 400) => {
          if (!statusCode)
            statusCode = err.code || 400;

          res.status(statusCode);
          res.json(err);
        }
        next();
      })
      // user authentication by accessToken
      server.use((req, res, next) => {
        // get user data
        handler.getUser(req.header('accessToken'), (err, user) => {
          if (err)
            return req.errorHandler(err);
            // check if user exists
          if (!user)
            return req.errorHandler({error: "user not found"}, 401);

          // set user data to request
          req.User = user;
          req.UserId = user.id;
          next()
        })
      })

      // create new url
      // body params:
      //    @param name String
      //    @param url String
      //    @param monitoredInterval: Integer
      // @response object {id: id Integer}
      server.post('/monitor', (req, res) => {
        var {name, url, monitoredInterval} = req.body;
        handler.create(req.UserId, {name, url, monitoredInterval}, (err, id) => {
          if (err) return req.errorHandler(err);
          res.json({id});
        })
      });

      // get all url data of specific user
      // @response Array of url data
      server.get('/monitor', (req, res) => {
        
        handler.getAll(req.UserId, (err, response) => {
          
          if (err) return req.errorHandler(err);

          res.json(response);
        })
      });

      // Delete url data from database by id
      // @param id Number
      // @response object {success: true}
      server.del('/monitor/:id', (req, res) => {
        handler.remove(req.UserId, req.params.id, err => {
          if (err) return req.errorHandler(err);
          
          res.json({success: true});
        })
      });

      // Update url data by id
      // @param id Number
      // body params:
      //    @param name String
      //    @param url String
      //    @param monitoredInterval: Integer
      // @response object {success: true}
      server.put('/monitor/:id', (req, res) => {
        var {name, url, monitoredInterval} = req.body;
        handler.update(req.UserId, req.params.id, {name, url, monitoredInterval}, err => {
          if(err) return req.errorHandler(err);
          
          res.json({success: true});
        })
      });

      // get last 10 results of url by id
      // @param urlId Number (id of url)
      server.get('/results/:urlId', (req, res) => {
        handler.getResults(req.UserId, req.params.urlId, (err, list) => {
          if (err) {
            console.error(err);
            return res.json(err)
          }
          res.json(list);
        })
      })
}