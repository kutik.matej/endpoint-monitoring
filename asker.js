const db = require('./databaseHandler');
const axios = require('axios');
const moment = require('moment');


module.exports = class Asker {
    constructor(onError, id, interval, url) {
        this.id = id;
        this.interval = interval;
        this.url = url;
        this.onError = onError;
        this.running = true;
    }
    // set asking interval 
    go() {
        setTimeout(() => {
            if (this.running) {
                this.ask();
                this.go();
            }
            
        }, this.interval * 1000);
    }
    // Send request and pass response to write() method
    ask() {
        var res;
        axios.get(this.url)
            .then(response => 
                res = response
            ).catch(({response}) =>
                res = response
            ).finally(() => {
                try {
                    this.write(res.status, res.data)
                } catch (err) {
                    this.onError(this, err);
                }
            })
    }
    // Save data to database and update MonitoredEndpoint data
    write(httpStatus, payload) {
        if (!httpStatus || !payload)
            return;

            payload = JSON.stringify(payload)

        var timeLog = moment().format('YYYY-MM-DD HH:mm:ss');
        db.insert('MonitoringResult', {httpStatus, /**payload,**/ monitoredEndpointId: this.id, createdAt: timeLog}, err => {
            if (err) return this.onError(this, err);
            
            db.update('MonitoredEndpoint', {id: this.id}, {lastCheck: timeLog}, err => {
                if (err) return this.onError(this, err);
            })
        })
    }
    // Stop object sending requests
    stop() {
        this.running = false;
    }
}