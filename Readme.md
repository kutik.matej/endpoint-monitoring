###Endpoint monitoring###

    ##Installation##

        #At first install npm packages.# 
            run: npm i

        #Then configure database configuration.#
            open file mySQL.config.js and change values of object

        #Now start app#
            run: npm start

    ##Usage##

        # App runs on http://localhost:3000 #

        # There must be accessToken of either User in header of each request.
        Use either "93f39e2f-80de-4033-99ee-249d92736a25" or "dcb20f8a-5657-4f1b-9f7f-ce65739b359".#

        # There are 5 router method #
            #POST "/monitor"#
                Create new Endpoint
                Body must contain parameters: 
                    @name: String = name of endpoint
                    @url: String = full url path 
                    @monitoredInterval: Integer = indicates delay between requests
                If successful response is json {success: true}
            
            #GET "/monitor"#
                Fetch all Endpoint created by user
                Response is array of Endpoint data in format { "id": Integer, "name": String, "url": String, "monitoredInterval": Integer, "createdAt": Date, "lastCheck": Date, "owner": Integer(users Id)
            
            #DELETE "/monitor/:id"#
                If user is owner of this Endpoint then delete Endpoint
                @parameter id: Number (Endpoints id)
                If successful response is json {success: true}

            #PUT "/monitor/:id
                If user is owner of this Endpoint then update Endpoint data
                @parameter id: Number (Endpoints id)
                Body contains one or more of parameters:
                    @name: String = name of endpoint
                    @url: String = full url path 
                    @monitoredInterval: Integer = indicates delay between requests
                If successful response is json {success: true}
            
            #GET "/results/:id"
                Fetch list of last 10 Results of Endpoint
                @parameter id: Number (Endpoints id)
                Response is array of Results data in format { "id": String, "createdAt": Date, "httpStatus": Integer, "payload": String,
                "monitoredEndpointId": Integer(Endpoints id)

        #For running tests run: npm test#
    }