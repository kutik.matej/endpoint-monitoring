const restify = require('restify'),
    router = require('./router.js'),
    bodyParser = require('body-parser'),
    AskerHandler = require("./AskerHandler")();

var server = restify.createServer();
server.use(bodyParser.urlencoded({ extended: false }))
 
server.use(bodyParser.json())

// Api
router(server)

server.listen(8000, function() {
  console.log('%s listening at %s', server.name, server.url);
//   start ask all url
  AskerHandler.start()
});
